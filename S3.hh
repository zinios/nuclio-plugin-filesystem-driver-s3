<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\fileSystem\driver\s3
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\fileSystem\driver\common\CommonInterface;

	use Aws\S3\S3Client;
	use Aws\S3\Exception;

	/**
	 * Manage file system functionalities on amazon web service (AWS) S3 bucket.
	 *
	 * This class provides the ability to manage CURD over files hosted on amazon web service (AWS) S3 bucket
	 * 
	 */
	<<provides('fileSystem::s3')>>
	class S3 extends Plugin implements CommonInterface
	{
		
		const S3URL = 's3.amazonaws.com';
		private S3Client $client;

		//should this be implemented here?
		public static function getInstance(...$args):S3
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}


		//http://<domainname>/objectname ?AWSAccessKeyId =<accesskey>& Expires =<expire time>& Signature =<signature string>
		//http://stage.socialite.app.s3.amazonaws.com/profile/avatar/80745d03-c295-4205-bd82-58161f2fd2d1-320.jpg
		//http://s3.amazonaws.com/johnsmith.net/homepage.html
		public function __construct(string $host, string $awsKey, string $awsSecretKey)
		{
			$this->client = S3Client::factory
			(
				[
					'base_url' => $host,
					'key'      => $awsKey,
					'secret'   => $awsSecretKey
				]
			);
			parent::__construct();
		}

		/**
		 * Returns the key name and the bucket name in the given S3 path. This methods assumes the path to be in one of the three forms given in the aws documentation
		 * http://docs.aws.amazon.com/AmazonS3/latest/dev/VirtualHosting.html, e.g:
		 * 	$path = 'http://johnsmith.eu.s3.amazonaws.com/homepage.html';
		 *  $path = 'http://s3.amazonaws.com/johnsmith.net/homepage.html';
		 * 	$path = 'http://www.johnsmith.net/homepage.html';
		 * 	any other ULR structure will return incorrect results.
		 * 
		 * @access public
		 * 
		 * @param  string $path The path to be translated into a key/bucket pair
		 * @return vector<string>       the key/bucket pair
		 */
		private function pathToKeyAndBucket(string $path):Vector<string>
		{
			if(strpos($path, self::S3URL)===false)
			{
				//assume the path is in the form $bucket/$key;
				$path = str_replace("http://", '', $path);
				$parts = explode('/', $path);
				$bucket = array_shift($parts);
				$file = implode('/',$parts);
			}
			else
			{
				$parts = [substr($path, 0, strpos($path, self::S3URL)), substr($path, strpos($path, self::S3URL)+strlen(self::S3URL))];
				$file = end($parts);
				$bucket = reset($parts);
				
				$file = preg_replace('|/|', '', $file, 1);
				$bucket = str_replace("http://", '', $bucket);
				$bucket = preg_replace('/.$/', '', $bucket);

				//if path is in the form 'http://johnsmith.eu.s3.amazonaws.com/homepage.html' then $file and bucket are already correct
				if($bucket === '')
				{
					//if path is in the form 'http://s3.amazonaws.com/johnsmith.net/homepage.html', $bucket will be empty and $file will be 'johnsmith.net/homepage.html'
					$parts = explode('/', $file);
					$bucket = reset($parts);
					$file = end($parts);
				}
			}

			return Vector{$file,$bucket};
		}

		/**
		 * Check if the file exist or not.
		 * 
		 *  @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return bool       	True/False for file existance.
		 */
		public function exists(string $path, array $options=array()):bool
		{
			list($key,$bucket) = $this->pathToKeyAndBucket($path);

			return $this->client->doesObjectExist($bucket, $key, $options);
		}

		/**
		 * Check if the path given is a file.
		 * 
		 *  @access public
		 * 
		 * @param  string  $path Path to the file.
		 * @return boolean       True/False for file or not.
		 */
		public function isFile(string $path):bool
		{
			return false;
		}

		/**
		 * Check whether the param given is path or not.
		 * 
		 *  @access public
		 * 
		 * @param  string  $directory Path to a directory
		 * @return boolean            True/False for the path or not.
		 */
		public function isDirectory(string $directory):bool
		{
			return false;
		}

		/**
		 * Determine if empty.
		 * 
		 * @access public
		 *
		 * @param      string   $directory  Path of directory
		 *
		 * @return     boolean  True if empty, False otherwise.
		 */
		public function isEmpty(string $directory):bool
		{
			return false;
		}

		/**
		 * Get file or folder information such as
		 * (type, and size)
		 *
		 * @access public
		 * 
		 * @param      string  $path   path of the file or folder
		 *
		 * @return     Map     Object meta.
		 */
		public function getObjectMeta(string $path):?Map<string,string>
		{
			list($key,$bucket) = $this->pathToKeyAndBucket($path);
			$result = $this->client->getObject
			(
				array
				(
					'Bucket' => $bucket,
					'Key' => $key,
					'Range' => 0,
				)
			);
			return new Map($result->toArray());
			// if($result['ContentType'] && $result['Metadata'] && $result['ContentLength']) //TODO: check if content length is for the object or for the returned chunk (because I asked for 0)
			// {
			// 	$metadata = new Map($result['Metadata']);
			// 	$metadata->add(Pair {'type', $result['ContentType'] });
			// 	$metadata->add(Pair {'size', $result['ContentLength'] });
			// 	return  $metadata;
			// }
			// return null;
		}

		/**
		 * Get the permissions applied on file or folder.
		 * 
		 * @access private
		 *
		 * @param      string  $path   path of the file or folder
		 *
		 * @return     <type>  Permissions.
		 */
		private function getPermissions(string $path):vector
		{
			list($key, $bucket) = $this->pathToKeyAndBucket($path);
			$result = $this->client->getObjectAcl
			(
				array
				(
					'Bucket' => $bucket,
					'Key' => $key
				)
			);
			$permissions = Vector();
			foreach ($result['Grants'] as $grant) 
			{
				// if($grant['Grantee']['ID']==$this->id) //TODO:????
				// {
					$permissions[]=$grant['Permission'];
				// }
			}
			return $permissions;
		}

		/**
		 * Check if the given path is writeable.
		 * 
		 * @access public 
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isReadable(string $path):bool
		{
			$permissions = $this->getPermissions($path);
			return (in_array('READ', $permissions) || in_array('FULL_CONTROL', $permissions));
		}

		/**
		 * Check if the given path is writeable.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isWritable(string $path):bool
		{
			$permissions = $this->getPermissions($path);	
			return (in_array('WRITE', $permissions) || in_array('FULL_CONTROL', $permissions));
		}

		/**
		 * Delete file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file to be deleted.
		 * @return mixed       	True if delete success, error message if failed.
		 */
		public function delete(string $path, int $rules):bool
		{
			list($key,$bucket) = $this->pathToKeyAndBucket($path);
			$result = $this->client->deleteObject
			(
				array
				(
					'Bucket' => $bucket,
					'Key' => $key
				)
			);
			if($result)
			{
				return true;
			}
			//if deleteObject failed maybe the passed path points t oa bucket. Try to delete the bucket
			elseif(	$this->client->doesBucketExist($path) )
			{
				$result = $this->client->deleteBucket
				(
					[
						'Bucket' => $path,
					]
				);
			}
			if($result)
			{
				return true;	
			}
			else
			{
				return false;	//TODO:check $result format
			}
		}

		/**
		 * Move file to another place.
		 * 
		 * @access public
		 * 
		 * @param  string $path   	File to be moved.
		 * @param  string $target 	Where to move.
		 * @return bool         	True/False for the move process.
		 */
		public function move(string $path, string $target, int $rules):bool
		{
			$copyresult=$this->copy($path, $target);
			if($copyresult)
			{
				return $this->delete($path, $rules);	
			}
			else return $copyresult;
		}

		/**
		 * Copy file to another place.
		 * 
		 * @access public
		 * 
		 * @param  string $path   	File to be copied.
		 * @param  string $target 	Where to copied.
		 * @return bool         	True/False for the copied process.
		 */
		 
		public function copy(string $path, string $target):bool
		{
			list($sourceKey,$sourceBucket)=$this->pathToKeyAndBucket($path);
			list($targetKey,$targetBucket)=$this->pathToKeyAndBucket($target);
			$result = $this->client->copyObject
			(
				[
					'CopySource' => $sourceBucket.'/'.$sourceKey,
					'Bucket' => $targetBucket,
					'Key' => $targetKey
				]
			);
			if($result){return true;}else{return false;}
		}

		/**
		 * Creates a new empty directory/bucket at the given path
		 * 
		 * @access public
		 * 
		 * @param string $path 		Path to be created.
		 * @return  bool  			True/false on success/failure
		 */
		public function create(string $path,?string $data=null):bool
		{
			if (is_null($data))
			{
				//TODO: Create bucket/folder.
			}
			else
			{
				$path		=explode('/',$path);
				$last		=end($path);
				$bucket		=array_shift($path);
				$key		=implode('/',$path);
				$endParts	=explode('.',$last,2);
				$extension	=end($endParts);
				
				switch ($extension)
				{
					case 'jpg':	$contentType='image/jpeg';	break;
					case 'png':	$contentType='image/png';	break;
					case 'gif':	$contentType='image/gif';	break;
					default:	$contentType='binary/octet-stream';
				}
				$result = $this->client->putObject
				(
					[
						'Bucket'		=>$bucket,
						'Key'			=>$key,
						'Body'			=>$data,
						'ACL'			=>'public-read',
						'ContentType'	=>$contentType
					]
				);
				return true;
			}
			return false;
			
			
			
			
			list($key,$bucket) = $this->pathToKeyAndBucket($path);
			$result = false;
			if($key==='' || $bucket === '')
			{
				//not a valid path, try to create bucket
				if($result = $this->client->isValidBucketName($path))
				{
					$result = $this->client->createBucket
					(
						array
						(
							'Bucket' => $path
						)
					);
				}
			}
			else
			{
				$result = $this->client->putObject
				(
					array
					(
						'Bucket' => $bucket,
						'Key'    => $key
					)
				);	
			}
			if($result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/**
		 * List all buckets in the S3
		 * 
		 * @return list of buckets
		 */
		public function listAllBuckets ():Vector<string>
		{
			$result = $this->client->listBuckets();
			$buckets = new Vector();
			foreach ($result['Buckets'] as $bucket) 
			{
			   // Each Bucket value will contain a Name and CreationDate
				$file = Map{};
				$file->add(Pair{'Key',"{$bucket['Name']}/"});

				$file->add(Pair{'dirName'		,'.'});
				$file->add(Pair{'baseName'		,"{$bucket['Name']}"});
				$file->add(Pair{'fileName'		,"{$bucket['Name']}"});
				$file->add(Pair{'fullName'		,'/'."{$bucket['Name']}" .'/'});

				$file->add(Pair{'extension', null});
				$file->add(Pair{'Size', 0});
				$file->add(Pair{'isFile', false});
				$file->add(Pair{'isDir', true});
				$file->add(Pair{'LastModified', ''});
				

				$buckets [] = $file;
			}

			 return $buckets;
		}


		/**
		 * Look for the folder size for the given path
		 * 
		 * @param  string path
		 * @return string the size with appropriate unit name
		 */

		public function folderSize ($path):string
		{
			$path=explode('/',$path,2);
			$size = 0;
			$bucket = $path[0];
			$folder = $path[1];
			
			$objects = $this->client->getIterator('ListObjects', array("Bucket" => $bucket, "Prefix" => $folder));
			$i = 0;
			foreach ($objects as $object) {
				$size = $size+$object['Size'];
			}
			
			return $this->formatSizeUnits($size);

		}


		/**
		 * change the format of the incoming bytes to appropriate unit name
		 * 
		 * @param  string bytes
		 * @return string the size with appropriate unit name
		 */
		private function formatSizeUnits($bytes) 
		{
			if ($bytes >= 1073741824) 
			{
				$bytes = number_format($bytes / 1073741824, 2) . ' GB';
			} 
			elseif ($bytes >= 1048576) 
			{
				$bytes = number_format($bytes / 1048576, 2) . ' MB';
			} 
			elseif ($bytes >= 1024) 
			{
				$bytes = number_format($bytes / 1024, 2) . ' KB';
			} 
			elseif ($bytes > 1) 
			{
				$bytes = $bytes . ' bytes';
			} 
			elseif ($bytes == 1) 
			{
				$bytes = $bytes . ' byte';
			} 
			else 
			{
				$bytes = '0 bytes';
			}
			return $bytes;
		}

		/**
		 * Retruns the contents of the folder at the passed path
		 * 
		 * @access public
		 * 
		 * @param  string $path 
		 * @return string       contents of the given path
		 */
		public function listAllFilesAndFolders(string $path):Vector<string>
		{
			$strPath = $path;
			$path=explode('/',$path,2);
			if ($path[1]!=='')
			{
				$path[1]=trim($path[1],'/').'/';
			}
			$result=$this->client->listObjects
			(
				[
					'Bucket'	=>$path[0],
					'Prefix'	=>$path[1]
				]
			);
			$list=Vector{};
			$result=new Map($result);
			if ($result->containsKey('Contents'))
			{
				foreach ($result->get('Contents') as $file) 
				{
					if ($file['Key']!=$path[1])
					{
						$info=pathinfo($file['Key']);

						$file=array_merge
						(
							$file,
							[
								'dirName'		=>$info['dirname'],
								'baseName'		=>$info['basename'],
								'fileName'		=>$info['filename'],
								'fullName'		=>'/'.$strPath.$file['Key'],
								'lastModified'	=>$file['LastModified']
							]
						);
						$newKey		=str_replace($path[1],'',$file['Key']);
						$length		=strlen($newKey)-1;
						$position	=strpos($newKey,'/');
						if ($position)
						{
							if ($position!==$length)
							{
								continue;
							}
							$file['extension']	=null;
							$file['size']		=0;
							$file['isFile']		=false;
							$file['isDir']		=true;
						}
						else
						{
							$file['extension']	=$info['extension'];
							$file['size']		=$file['Size'];
							$file['isFile']		=true;
							$file['isDir']		=false;
						}
						$file['Key']=str_replace($path[1],'',$file['Key']);
						$list[]=$file;
					}
				}
			}
			return $list;
		}

		/**
		 * Returns a list of all the files/folders matching the given regex string inside the given path.
		 * 
		 * @access public
		 * 
		 * @param  string $path  Path to perform the search into.
		 * @param  string $regex regex expression to be matched
		 * @return string List of all the search results
		 */
		public function find(string $path, string $regex):Vector<string>
		{
			return Vector{};
		}

		/**
		 * Delete all files inside directory.
		 * 
		 * @access public
		 * 
		 * @param  string $directory 	Path to directories
		 * @return bool            		True/False for successfully deleted.
		 */
		public function deleteAllFilesInPath(string $path):bool
		{
			list($key, $bucket) = $this->pathToKeyAndBucket($path);
			end(str_split($key))==='/'?null:$key=$key.'/';
			$result = $this->client->deleteMatchingObjects($bucket, $key);
			if($result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/**
		 * Get content of a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function read(string $path):string
		{
			$content='';
			list($file,$bucket) = $this->pathToKeyAndBucket($path);
			try
			{
				// Get the object
				$result = $this->client->getObject
				(
					[
						'Bucket' => $bucket,
						'Key'    => $file
					]
				);
				
				$result['Body']->seek(0);
				$content=$result['Body']->read($result['Body']->getSize());
			}
			catch (S3Exception $e) 
			{
				//handle
			}
			return $content;
		}

		/**
		 * Write content to a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function write(string $path, string $content):bool
		{
			list($key, $bucket)=$this->pathToKeyAndBucket($path);
			$result = $this->client->putObject
			(
				[
					'Bucket' => $bucket,
					'Key'    => $key,
					'Body'   => $content
				]
			);
			if($result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/**
		 * Write content to a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function append(string $path, string $contents):mixed
		{
			if(!$this->exist($path))
			{
				return false;
			}
			$body = $this->read($path);
			if(!is_string($body))
			{
				return 'Cannot append.'; //TODO: check for alternatives.
			}
			else
			{
				$newBody = $body . $contents;
				return $this->write($path, $newBody);
			}
		}

		/**
		 * Creates an empty file at hte passed path
		 * 
		 * @access public
		 * 
		 * @param  string $path the path to the file to be created
		 * @return bool       	true/false on success/failure
		 */
		public function touch(string $path):bool
		{
			list($key, $bucket)=$this->pathToKeyAndBucket($path);
			$result = $this->client->putObject
			(
				[
					'Bucket' => $bucket,
					'Key'    => $key,
					'Body'   => ''
				]
			);
			if($result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/**
		 * Get the file name.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File name
		 */
		public function getName(string $path):?string
		{
			list($key, $bucket) = $this->pathToKeyAndBucket($path);
			if($this->client->doesObjectExists($bucket, $key))
			{
				return reset(explode('.',$key));
			}
			return null;
		}

		/**
		 * Get the file extension.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File extension
		 */
		public function getExt(string $path):?string
		{
			list($key, $bucket) = $this->pathToKeyAndBucket($path);
			if($this->client->doesObjectExists($bucket, $key))
			{
				return end(explode('.',$key));
			}
			return null;
		}

		/**
		 * Get the file type.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File type
		 */
		public function getType(string $path):?string
		{
			$meta = $this->getObjectMeta($path);
			if(!is_null($meta) && array_key_exists('type', $meta))
			{
				return $meta['type'];
			}
			else
			{
				return null;
			}
		}

		/**
		 * Get the file size.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return int      	File size
		 */
		public function getSize(string $path):?int
		{
			$meta = $this->getObjectMeta($path);
			if(!is_null($meta) &&  array_key_exists('size', $meta))
			{
				return intval($meta['size']);
			}
			else
			{
				return null;
			}
		}




	}
}
